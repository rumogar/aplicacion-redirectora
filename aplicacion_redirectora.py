import socket
import random


myPort = 1234
mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
mySocket.bind(('', myPort))


mySocket.listen(5)

# Initialize random number generator (not exactly needed, since the
# import of the module already is supposed to do that). No argument
# means time is used as seed (or OS supplied random seed).
random.seed()

# Accept connections, read incoming data, and answer back an HTLM page
#  (in a loop)
firstUrl = ["https://gitlab.etsit.urjc.es", "https://labs.etsit.urjc.es/", "https://www.aulavirtual.urjc.es/moodle/"]
try:
	while True:
		print("Waiting for connections")
		(recvSocket, address) = mySocket.accept()
		print("HTTP request received:")
		print(recvSocket.recv(2048))

		# Resource name for next url
		nextUrl = random.choice(firstUrl)
		redic = "HTTP/1.1 302 Temporary Redirect \r\n" \
        	+ "Location: " + nextUrl + "\r\n\r\n"
		recvSocket.send(redic.encode('ascii'))
		recvSocket.close()

except KeyboardInterrupt:
	print("Closing binded socket")
	mySocket.close()
